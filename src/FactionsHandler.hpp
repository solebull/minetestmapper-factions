#ifndef __FACTIONS_HPP__
#define __FACTIONS_HPP__

// The real added feature of this mapper : the factions mod handling
//
//

#include <string>
#include <list>

// Forward declarations
class lua_State;
class Faction;
// End of Forward declarations

/** The main factions handler
  *
  * Will try to read the world's factions.conf file.
  *
  */
class FactionsHanlder
{
public:
  FactionsHanlder(const std::string &);

  std::string getFactionsFilePath(void);

protected:
  std::string luaTypeToStr(int) const;
  void        printLuaTable(lua_State *);

  
private:
  /** The absolute full path to the factions.conf file
    *
    */
  std::string factions_file_path;  
  std::list<Faction*> factionList; //!< A list of discovered factions
};

#endif /* !__FACTIONS_HPP__ */
