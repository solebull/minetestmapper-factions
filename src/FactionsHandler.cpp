/** 
 * The real added feature of this mapper : the factions mod handling
 *
 */

#include "FactionsHandler.hpp"

#include <iostream>
#include <sstream>

#include <lua.h>

#include <lualib.h>
#include <lauxlib.h>

#include <assert.h> // Uses assert()

#include "Faction.hpp"
#include "FactionsFileNotFound.hpp"

#include <filesystem>

using namespace std;

static void
iterate_and_print(lua_State *L, int index)
{
    // Push another reference to the table on top of the stack (so we know
    // where it is, and this function can work for negative, positive and
    // pseudo indices
    lua_pushvalue(L, index);
    // stack now contains: -1 => table
    lua_pushnil(L);
    // stack now contains: -1 => nil; -2 => table
    while (lua_next(L, -2))
    {
        // stack now contains: -1 => value; -2 => key; -3 => table
        // copy the key so that lua_tostring does not modify the original
        lua_pushvalue(L, -2);
        // stack now contains: -1 => key; -2 => value; -3 => key; -4 => table
        const char *key = lua_tostring(L, -1);
        const char *value = lua_tostring(L, -2);
	
	if (value == NULL)
	  iterate_and_print(L, -2);
	    
        printf("%s => %s\n", key, value);
        // pop value + copy of key, leaving original key
        lua_pop(L, 2);
        // stack now contains: -1 => key; -2 => table
    }
    // stack now contains: -1 => table (when lua_next returns 0 it pops the key
    // but does not push anything.)
    // Pop table
    lua_pop(L, 1);
    // Stack is now the same as it w
}

/** Just a basic function used to dump/test for the lua stack
  * call this function with the table on the top of the stack
  *
  * \param L The lua state object
  *
  */
void
luaAccess(lua_State * L)
{
  assert(L && "Invalid Lua state");
  
  cout << "=> Entering luaAccess()" << endl;
  lua_pushnil(L);
  while(lua_next(L, -2) != 0)
    {
      switch(lua_type(L, -2))
	{
	case LUA_TSTRING:
	  cout << "Lua object is a string" << endl;
	  //deal with the outer table index here
	  break;
	  //...  
	  // deal with aditional index types here
	}
      switch(lua_type(L, -1))
	{
	case LUA_TTABLE:
	  cout << "Lua object is a table" << endl;
	  lua_pushnil(L);
	  while (lua_next(L, -1))
	    {
	      switch(lua_type(L, -2))
		{
		case LUA_TNUMBER:
		  // deal with the inner table index here
		  break;
		  //...  
		  // deal with aditional index types here
		}
	      switch(lua_type(L, -1))
		{
		case LUA_TSTRING:
		  cout << "Lua object is a string" << endl;
		  //deal with strings
		  break;
		case LUA_TNUMBER:
		  cout << "Lua object is a number" << endl;
		  //deal with numbers
		  break;
		}
	      lua_pop(L, 1);
	    }
	  //...  
	  // deal with aditional index types here
	  break;
	}
    }
  lua_pop(L, 1);
}


/** The constructor
  *
  * \param worldpath The worldpath from the mapper.cpp file
  *
  */
FactionsHanlder::FactionsHanlder(const std::string & worldpath):
  factions_file_path(worldpath + "factions.conf")
{
  // Handles the special case where the user doesn't give a full path
  // (without the last '/' character)
  if (worldpath.back() != '/')
    factions_file_path = worldpath + "/factions.conf";
    
  if (worldpath.back() == '\n')
    cout << "EOL is a newline char" << endl;
    
  // Convert relative path to canonical
  factions_file_path = std::filesystem::absolute(factions_file_path);

  try
    {
      factions_file_path = std::filesystem::canonical(factions_file_path);
    }
  catch(const std::exception& ex)
    {
      cerr << endl <<  "Can't get canonical path for '" << factions_file_path
	   << "' :" << ex.what() << endl;
      throw FactionsFileNotFound(factions_file_path);
    }
  // Opens a lua interpreter
  lua_State *L = luaL_newstate();
  luaL_openlibs(L);
  
  // do stuff with Lua VM. In this case just load and execute a file:
  int df = luaL_dofile(L, factions_file_path.c_str());
  if (df != 0)
    throw FactionsFileNotFound(factions_file_path);

  /*
  lua_pcall(L, 0, LUA_MULTRET, 0);
  int stackLength = lua_gettop(L);
  cout << "Factions file stack length is " << stackLength << endl;

  int tableType = lua_type(L, -1);
  if ( tableType != LUA_TTABLE)
    {
      cout <<  "lua stack must contain a table" << endl;
      cout << "Found type is " << luaTypeToStr(tableType) << endl;

      // is a string
      if (tableType = LUA_TSTRING)
	{

	  const char* str = lua_tostring (L, -1);
	  cout << "ERROR: Found string is '" << str << "'" << endl;
	  exit(1);
	}
    }
  
  //lua_gettable(L, -1);

  //auto windowRef = luabridge::LuaRef::fromStack(L, -1);
  luaAccess(L);
  
  //  cout << luaL_getresult(1) << endl;

  // done? Close it then and exit.//
  lua_close(L);
*/

  /*
  // LoadFile test
  int status = luaL_loadfile(L, factions_file_path.c_str());
  if (status)
    {
      //status = docall(L, 0, LUA_MULTRET);
      cout << "ERROR loading lua file" << endl;
    }
    else */
  {
    cout << "loadfile worked!" << endl;
    //lua_call(L, 0, 1);

    int stackLength = lua_gettop(L);
    cout << "Factions file stack length is " << stackLength << endl;
    int tableType = lua_type(L, -1);
    cout <<  "lua stack must contain a table" << endl;
    cout << "Found type is " << luaTypeToStr(tableType) << endl;

    iterate_and_print(L, -1);
    
    //    printLuaTable(L);
    const char* str = lua_tostring (L, -1);
    cout << "ERROR: Found string is '" << str << "'" << endl;

  }
  
}

/** Return the printable name of a lua string
  *
  * \param t The lua type, for example returned by lua_type()
  *
  * \return A printable string.
  *
  */
string
FactionsHanlder::luaTypeToStr(int t) const
{
  ostringstream oss;
  string ret;

  oss << '(' << t << ')';
  
  switch (t)
    {
    case LUA_TNONE:
      ret = "None";
      break;

    case LUA_TNIL:
      ret = "Nil";
      break;

    case LUA_TBOOLEAN:
      ret = "Boolean";
      break;

    case LUA_TLIGHTUSERDATA:
      ret = "Light user data";
      break;

    case LUA_TNUMBER:
      ret = "Number";
      break;

    case LUA_TSTRING:
      ret = "String";
      break;

    case LUA_TTABLE:
      ret = "Table";
      break;

    case LUA_TFUNCTION:
      ret = "Function";
      break;

    case LUA_TUSERDATA:
      ret = "Userdata";
      break;

    case LUA_TTHREAD:
      ret = "Thread";
      break;
    }

  oss << ret;
  return oss.str();
}

void
FactionsHanlder::printLuaTable(lua_State *L)
{
  lua_pushnil(L);

  while(lua_next(L, -2) != 0)
    {
      if(lua_isstring(L, -1))
	printf("%s = %s\n", lua_tostring(L, -2), lua_tostring(L, -1));
      else if(lua_isnumber(L, -1))
	printf("%s = %d\n", lua_tostring(L, -2), lua_tonumber(L, -1));
      /*      else if(lua_istable(L, -1))
	printLuaTable(L);
      */
      lua_pop(L, 1);
    }
}

/** Return the absolute path from factions_file_path
  *
  * \return The path a std::string.
  *
  */
string
FactionsHanlder::getFactionsFilePath(void)
{
  return factions_file_path;
}
