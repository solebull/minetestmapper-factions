#include "FactionsFileNotFound.hpp"

using namespace std;

FactionsFileNotFound::FactionsFileNotFound(const string& vFilename):
runtime_error("UNSET")
{
  msg = "Can't find file '" + vFilename + "'";
}

/** Destructor
  *
  */
FactionsFileNotFound::~FactionsFileNotFound()
{

}

/** Returns the current error message
  *
  * \return The msg string value.
  *
  */
const char*
FactionsFileNotFound::what() const throw()
{
  return msg.c_str();
}
