#ifndef __FACTIONS_FILE_NOT_FOUND_HPP__
#define __FACTIONS_FILE_NOT_FOUND_HPP__

#include <stdexcept> // USES std::runtime_error

#include <string>

/** An exception thrown in acse we can't find the Factions file
  *
  */
class FactionsFileNotFound : public std::runtime_error
{
public:
  FactionsFileNotFound(const std::string& vFilename);
  ~FactionsFileNotFound();

  virtual const char* what() const throw();
  
private:
  std::string msg; //!< The what() message string
};

#endif // !__FACTIONS_FILE_NOT_FOUND_HPP__
