#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include <catch.hpp>

#include "FactionsHandler.hpp"

#include "FactionsFileNotFound.hpp"

#include <iostream>

using namespace std;

/** Mainly to test ctor parameter etc...
  *
  */
TEST_CASE( "constructor_throw", "FactionsHandler" ) {
  FactionsHanlder* fh;
  REQUIRE_THROWS_AS(fh = new FactionsHanlder("Aze"), FactionsFileNotFound);
}

// Relative file containing '..' should not contain \n character
TEST_CASE( "filepath_relative_nonewline", "FactionsHandler" ) {
  FactionsHanlder* fh = new FactionsHanlder("../example/");
  auto ffp = fh->getFactionsFilePath();

  cerr << "ffp='" << ffp << "'" << endl;
  
  ffp.replace(ffp.find("\n"), string("\n").size(), "");

  cerr << "ffp='" << ffp << "'" << endl;

  REQUIRE( ffp.find("\n") == string::npos );
}

// Relative file handling should remove '..'
TEST_CASE( "filepath_relative", "FactionsHandler" ) {
  FactionsHanlder* fh = new FactionsHanlder("../example/");
  auto ffp = fh->getFactionsFilePath();

  REQUIRE( ffp.find("..") == string::npos );
}

// Shouldn't throw an exception with existing relative directory
TEST_CASE( "constructor_relative", "FactionsHandler" ) {
  FactionsHanlder* fh;
  REQUIRE_NOTHROW(fh = new FactionsHanlder("../example/"));
}

// Running in 'aze' path should throw an exception
TEST_CASE( "constructor", "FactionsHandler" ) {
  REQUIRE_THROWS_AS(new FactionsHanlder("aze"), FactionsFileNotFound);
}

