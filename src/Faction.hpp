#ifndef __FACTION_HPP__
#define __FACTION_HPP__

#include "Image.h" // Uses Color

#include <string>

/** Defines a faction as shown in the drawn map
  *
  *
  */
class Faction
{
public:
  Faction(const std::string&);

protected:
  void randomizeColor();
  
private:
  std::string name{};    //!< The name of this faction
  Color       color{};    //!< Actual color of this faction
};

#endif  // !__FACTION_HPP__
