#include "Faction.hpp"

#include <iostream>

using namespace std;

/** Named constructor
  *
  * Create a new faction with the given name and a randomized color.
  *
  * \param vName The name of the new faction.
  *
  */
Faction::Faction(const string& vName):
  name(vName)
{
  randomizeColor();
  
}


/** Change to a new randomized color
  *
  */
void
Faction::randomizeColor()
{
  int r = std::rand() % 255;
  int g = std::rand() % 255;
  int b = std::rand() % 255;
  this->color = Color(r, g, b);
  cout << "Color(" << r << ", " << g << ", " << b << ")" << endl;
}
