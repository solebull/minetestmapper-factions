return {
  ["newFaction"] = {
    ["allies"] = {},
    ["enemies"] = {},
    ["maxpower"] = 10,
    ["banner"] = "bg_white.png",
    ["is_admin"] = false,
    ["join_free"] = false,
    ["invited_players"] = {},
    ["power"] = 0.125,
    ["default_leader_rank"] = "leader",
    ["usedpower"] = 0,
    ["ranks"] = {
      ["leader"] = {"disband", "claim", "playerslist", "build",
                    "description", "ranks", "spawn", "banner", "promote"},
      ["member"] = {"build"},
      ["moderator"] = {"claim", "playerslist", "build", "spawn"}
    },
    ["default_rank"] = "member",
    ["last_logon"] = 1622742399,
    ["description"] = "Default faction description.",
    ["land"] = {},
    ["leader"] = "singleplayer",
    ["players"] = {
      ["singleplayer"] = "singleplayer"
    },
    ["name"] = "newFaction",
    ["attacked_parcels"] = {}
  }
}
